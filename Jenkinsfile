pipeline {
    agent none; 

    options { buildDiscarder(logRotator(numToKeepStr: '5')) }

    environment {
       CONDA_DEPLOYER = credentials('Conda_deployment')
    }

    stages {
        stage('Build') {
          matrix {
            agent { node {label "${PLATFORM}" } }

            axes {
              axis {
                name 'PLATFORM'
                values 'ubuntu', 'macos-m1'
              }
            }

            stages {
              stage('Conda-build (ubuntu)') {
                when {
                  environment name:'PLATFORM', value: 'ubuntu'
                }
                steps {
                  ansiColor('xterm') {
                    timeout(time: 240, unit: 'MINUTES') {
                      sh '''
                        . /build/jenkins/miniconda3/envs/builder/bin/activate
                        conda-build .
                      '''
                    }
                  }
                }
              }
              stage('Conda-build (macos-m1)') {
                when {
                  environment name:'PLATFORM', value: 'macos-m1'
                }
                steps {
                  ansiColor('xterm') {
                    timeout(time: 240, unit: 'MINUTES') {
                      sh '''
                        . /Users/jenkins/miniconda3/envs/builder/bin/activate
                        conda-build .
                      '''
                    }
                  }
                }
              }

              stage("Conda deploy") {
                steps {
                  sh """
                    if [ x${PLATFORM} = xubuntu ]; then
                      basepath=/build/jenkins
                      arch=linux-64
                    elif [ x${PLATFORM} = xmacos-m1 ]; then
                      basepath=/Users/jenkins
                      arch=osx-arm64
                    else
                      exit 1
                    fi
                    tar -C \$basepath/miniconda3/envs/builder/conda-bld/ -zcf conda.tgz \${arch} noarch
                    curl -v -F filename=conda -F file=@conda.tgz http://athos.iap.fr:9595/deploy-conda/${CONDA_DEPLOYER}
                  """
                }
              }
            }
          }
        }
    }
    post {
        failure {
            notifyBuild("FAIL")
        }
        success {
            notifyBuild("SUCCESS")
            sh """
              . /build/jenkins/miniconda3/envs/builder/bin/activate
              conda build purge
            """
        }
        cleanup {

            /* clean up our workspace */
            //deleteDir()
            /* clean up tmp directory */

            dir("${WORKSPACE}/${BUILD_TAG}") {
                deleteDir()
            }
            dir("${workspace}@tmp") {
                deleteDir()
            }
        }
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
  // build status of null means successful
  buildStatus = buildStatus ?: 'SUCCESS'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL})"

  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#0000FF'
  } else if (buildStatus == 'SUCCESS') {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  def details = """<p>STARTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    <p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>
    <p>Build status: <span style='color: ${colorCode};'>${buildStatus}</span></p>"""

  emailext (
      mimeType: 'text/html',
      subject: subject,
      body: details,
      recipientProviders: [developers(), requestor()]
    )
}
