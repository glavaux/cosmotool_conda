IFS=' ' read -ra SPLIT_CC <<< "$CC" 
IFS=' ' read -ra SPLIT_CXX <<< "$CXX" 
CC=${SPLIT_CC[0]}
CXX=${SPLIT_CXX[0]}

export CC=$(which $(basename ${CC}) )
export CXX=$(which $(basename ${CXX}) )

additional_arg=


mkdir build
( cd build; 
   cmake -DCMAKE_C_COMPILER=${CC} -DCMAKE_CXX_COMPILER=${CXX} \
         -DCMAKE_INSTALL_PREFIX=${PREFIX} \
         -DINTERNAL_GSL=OFF -DINTERNAL_BOOST=OFF -DINTERNAL_HDF5=OFF -DINTERNAL_NETCDF=OFF -DBUILD_PYTHON=ON ..;
    make -j2 install
)
